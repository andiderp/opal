/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package av
package checking

import scala.collection.Set

import org.opalj.br.ClassFile
import org.opalj.br.FieldType
import org.opalj.br.Field
import org.opalj.br.analyses.SomeProject
import org.opalj.br.VirtualSourceElement

/**
 * @author Marco Torsello
 * @author Michael Eichberg
 */
case class FieldMatcher(
    classLevelMatcher: Option[ClassLevelMatcher],
    annotation: Option[String], // needs to be reconsidered...
    fieldType: Option[FieldType],
    fieldName: Option[NameMatcher])
        extends SourceElementsMatcher {

    def doesClassFileMatch(classFile: ClassFile): Boolean = {
        classLevelMatcher.isEmpty || classLevelMatcher.get.doesMatch(classFile)
    }

    def doesFieldMatch(field: Field): Boolean = {
        (fieldType.isEmpty || (fieldType.get eq field.fieldType)) && (
            (fieldName.isEmpty || fieldName.get.doesMatch(field.name)))
    }

    def extension(project: SomeProject): Set[VirtualSourceElement] = {
        val allMatchedFields = project.allClassFiles collect {
            case classFile if doesClassFileMatch(classFile) ⇒ {
                classFile.fields collect {
                    case field if doesFieldMatch(field) ⇒ field.asVirtualField(classFile)
                }
            }
        }
        allMatchedFields.flatten.toSet
    }

}

object FieldMatcher {

    def apply(
        className: Option[ClassLevelMatcher] = None,
        annotation: Option[String] = None,
        fieldType: Option[String] = None, // java.lang.Object
        fieldName: String,
        matchPrefix: Boolean = false): FieldMatcher = {
        this(
            className,
            annotation,
            fieldType.map(ftn ⇒ FieldType(ftn.replace('.', '/'))),
            Some(SimpleNameMatcher(fieldName, matchPrefix)))
    }

}
