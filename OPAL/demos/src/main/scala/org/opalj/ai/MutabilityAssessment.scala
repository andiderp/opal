/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package ai

import java.net.URL

import org.opalj.ai.analyses.MutabilityRating.Unknown
import org.opalj.ai.analyses.ImmutabilityAnalysis
import org.opalj.br.analyses.AnalysisExecutor
import org.opalj.br.analyses.BasicReport
import org.opalj.br.analyses.OneStepAnalysis
import org.opalj.br.analyses.Project

/**
 * A analysis that collects all classes that are immutable inside a jar.
 *
 * @author Andre Pacak
 */
object MutabilityAssessment extends AnalysisExecutor with OneStepAnalysis[URL, BasicReport] {

    val analysis = this

    override def doAnalyze(
        theProject: Project[URL],
        parameters: Seq[String],
        isInterrupted: () ⇒ Boolean): BasicReport = {

        import org.opalj.util.PerformanceEvaluation.{ time, ns2sec }

        var message = ""
        val result =
            time {
                ImmutabilityAnalysis.doAnalyze(theProject, isInterrupted)
            } { t ⇒
                message += f"Analysis time: ${ns2sec(t)}%2.2f seconds. Result:\n"
            }
        val classesWithMutabilityRating = result //.filter(_._2 != Unknown)
        val classesPerMutabilityRating =
            classesWithMutabilityRating.groupBy(_._2). // grouped by mutability rating
                map(e ⇒ e._2.keys.map(_.toJava).mkString(e._1.toString+" ("+(e._2.size)+") :\n\t", ",\n\t", "\n"))

        message += classesPerMutabilityRating.mkString("\n")
        BasicReport(message)
    }

}